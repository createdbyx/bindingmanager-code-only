﻿namespace Codefarts.BindingManager
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public class BindingManager
    {
        private static BindingManager singleton;

        private static BindingManager Instance
        {
            get
            {
                if (singleton == null)
                {
                    singleton = new BindingManager();
                }

                return singleton;
            }
        }

        private struct BindingKey
        {
            public string Name;

            public INotifyPropertyChanged Source;
        }

        private IDictionary<BindingKey, IExecuter> bindings = new Dictionary<BindingKey, IExecuter>();
        private IDictionary<string, IExecuter> namedBindings = new Dictionary<string, IExecuter>();

        private interface IExecuter
        {
            void Execute();

            void Update();
            string Name { get; set; }
        }

        private class BindingModel<T> : IExecuter
        {
            public Action<T> SetLeftValue;
            public Action<T> SetRightValue;

            public Func<T> GetLeftValue;
            public Func<T> GetRightValue;

            public T PreviousLeftValue;
            public T PreviousRightValue;

            public void Execute()
            {
                this.SetLeftValue(this.GetLeftValue());
            }

            public void Update()
            {
                var leftValue = this.GetLeftValue();
                if (!leftValue.Equals(this.PreviousLeftValue))
                {
                    this.SetRightValue(leftValue);
                }

                var rightValue = this.GetRightValue();
                if (!rightValue.Equals(this.PreviousRightValue))
                {
                    this.SetLeftValue(rightValue);
                }

                this.PreviousLeftValue = leftValue;
                this.PreviousRightValue = rightValue;
            }

            public string Name { get; set; }
        }

        internal int DoGetCount()
        {
            return this.bindings.Count + this.namedBindings.Count;
        }

        public static int GetBindingCount()
        {
            return Instance.DoGetCount();
        }

        internal void DoBind<T>(INotifyPropertyChanged source, string name, Func<T> getValue, Action<T> setValue)
        {
            this.bindings.Add(new BindingKey() { Source = source, Name = name }, new BindingModel<T>() { Name = name, GetLeftValue = getValue, SetLeftValue = setValue });
            source.PropertyChanged += this.OnSourceOnPropertyChanged;
        }

        internal void DoBind<T>(string name, Func<T> leftValue, Func<T> rightValue, Action<T> setLeftValue, Action<T> setRightValue)
        {
            this.namedBindings.Add(name, new BindingModel<T>()
                {
                    Name = name,
                    GetLeftValue = leftValue,
                    SetLeftValue = setLeftValue,
                    GetRightValue = rightValue,
                    SetRightValue = setRightValue,
                });
        }

        public static void Update()
        {
            Instance.DoUpdateAll();
        }

        public static void Bind<T>(INotifyPropertyChanged source, string name, Func<T> getValue, Action<T> setValue)
        {
            Instance.DoBind(source, name, getValue, setValue);
        }

        public static void Bind<T>(string name, Func<T> leftValue, Func<T> rightValue, Action<T> setLeftValue, Action<T> setRightValue)
        {
            Instance.DoBind(name, leftValue, rightValue, setLeftValue, setRightValue);
        }

        private void OnSourceOnPropertyChanged(object s, PropertyChangedEventArgs e)
        {
            IExecuter model;
            if (this.bindings.TryGetValue(new BindingKey() { Source = s as INotifyPropertyChanged, Name = e.PropertyName }, out model))
            {
                if (e.PropertyName == model.Name)
                {
                    model.Execute();
                }
            }
        }

        internal void DoUnbindAll()
        {
            foreach (var pair in this.bindings)
            {
                this.DoUnbind(pair.Key.Source, pair.Key.Name);
            }

            this.namedBindings.Clear();
            this.bindings.Clear();
        }

        internal void DoUpdateAll()
        {
            foreach (var pair in this.namedBindings)
            {
                pair.Value.Update();
            }
        }

        public static void UnbindAll()
        {
            Instance.DoUnbindAll();
        }

        public static void Unbind(INotifyPropertyChanged source, string name)
        {
            Instance.DoUnbind(source, name);
        }

        internal void DoUnbind(INotifyPropertyChanged source, string name)
        {
            BindingKey? key = null;
            foreach (var binding in this.bindings)
            {
                if (binding.Key.Source == source && binding.Key.Name == name)
                {
                    key = binding.Key;
                    break;
                }
            }

            if (key == null)
            {
                return;
            }

            key.Value.Source.PropertyChanged -= this.OnSourceOnPropertyChanged;
        }

        public static void Unbind(string name)
        {
            Instance.DoUnbind(name);
        }

        internal void DoUnbind(string name)
        {
            string key = null;
            foreach (var binding in this.namedBindings)
            {
                if (binding.Key == name)
                {
                    key = binding.Key;
                    break;
                }
            }

            if (key == null)
            {
                return;
            }

            this.namedBindings.Remove(key);
        }
    }
}
